/*
* InterruptEncoderExample.c
*
* Created: 10/7/2018 4:45:52 PM
* Author : Ruben Castro
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "ATKBBBIO/ATKDAC.h"
#include "ATKBBBIO/RBasic.h"


int counter;

int main(void)
{
	dac_begin(ATK_DAC_CHANNEL_0);
	
	counter=2048;
	PORTE_DIRSET = PIN3_bm;
	PORTE_DIRSET |= PIN2_bm;

	PORTC_DIRSET &= ~(PIN2_bm | PIN3_bm);

	sei();
	PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
	
	PORTC.INTCTRL = PMIC_MEDLVLEN_bm;
	
	//interrupt 0 PC1 rising edge
	PORTC.INT0MASK = PIN1_bm;
	PORTC.PIN1CTRL = PORT_ISC_RISING_gc;
	
	//interrupt 1 PC2 falling edge
	PORTC.INT1MASK = PIN2_bm;
	PORTC.PIN2CTRL = PORT_ISC_RISING_gc;

	
	/* Replace with your application code */
	while (1)
	{
		PORTE_OUTSET = PIN2_bm;
		for(int i =0;i<20000;i++);
		PORTE_OUTCLR = PIN2_bm;
		for(int i =0;i<20000;i++);
	}


}

//PC1 is Rising -1
ISR(PORTC_INT0_vect)
{
	if(!(PORTC_IN & PIN2_bm)){
		counter+=10;
	}else{
		counter-=10;
	}
	set_dac(ATK_DAC_CHANNEL_0, CONSTRAIN(counter,0,4096));
}

//PC2 is falling -0
ISR(PORTC_INT1_vect)
{
	if((PORTC_IN & PIN1_bm)){
		counter+=10;
	}else{
		counter-=10;
	}
	set_dac(ATK_DAC_CHANNEL_0, CONSTRAIN(counter,0,4096));
}




