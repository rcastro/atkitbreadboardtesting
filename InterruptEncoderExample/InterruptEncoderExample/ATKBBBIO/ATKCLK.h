/*
 * ATKCLK.h
 *
 * Created: 10/7/2018 2:02:03 PM
 *  Author: Jake Read and Ruben Castro
 */ 


#ifndef ATKCLK_H_
#define ATKCLK_H_

 void clock_init(void);

#endif /* ATKCLK_H_ */