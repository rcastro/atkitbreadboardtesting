/*
 * ATKPWM0.h
 *
 * Created: 10/7/2018 3:03:49 PM
 *  Author: Ruben Castro
 */ 


#ifndef ATKPWM0_H_
#define ATKPWM0_H_
#define ATK_BASE_FREQUENCY 48000000

typedef enum ATK_PWM_CHANNEL_enum{
	ATK_PWM_CHANNEL_A =0x01<<4,
	ATK_PWM_CHANNEL_B =0x01<<5,
	ATK_PWM_CHANNEL_C =0x01<<6,
	ATK_PWM_CHANNEL_D = 0x01<<7,
} ATK_PWM_CHANNEL_sel;

 void set_pwm(ATK_PWM_CHANNEL_sel channel, uint16_t val);
void set_pwm_dutycycle(ATK_PWM_CHANNEL_sel channel, double val);
 void pwm_setPrescalar(TC_CLKSEL_t prescalar);
 int pwm_begin(ATK_PWM_CHANNEL_sel channel, TC_CLKSEL_t prescalar, int desiredFrequency);



#endif /* ATKPWM_H_ */