/*
* ATKPWM.c
*
* Created: 10/7/2018 3:04:22 PM
*  Author: Ruben Castro
*/

#include "avr/io.h"
#include "RBasic.h"
#include "ATKPWM0.h"
#include <math.h>


uint16_t per = 1000;

void set_pwm_dutycycle(ATK_PWM_CHANNEL_sel channel, double val){ //setPWM to value between 0 and 100
	uint16_t pwm = (CONSTRAIN(val,0.0,100.0)/100.0) * per;
	set_pwm(channel,pwm);
}

void set_pwm(ATK_PWM_CHANNEL_sel channel, uint16_t val){
	uint8_t vall = (uint8_t) val;
	uint8_t valh = (uint8_t) (val >> 8);

	if(channel & (ATK_PWM_CHANNEL_A)){
		TCC0.CCABUFL = vall;
		TCC0.CCABUFH = valh;
		}else if(channel & (ATK_PWM_CHANNEL_B)){
		TCC0.CCBBUFL = vall;
		TCC0.CCBBUFH = valh;
		}else if(channel & (ATK_PWM_CHANNEL_C)){
		TCC0.CCCBUFL = vall;
		TCC0.CCCBUFH = valh;
		}else if(channel & (ATK_PWM_CHANNEL_D)){
		TCC0.CCDBUFL = vall;
		TCC0.CCDBUFH = valh;
	}
}

void pwm_setPrescalar(TC_CLKSEL_t prescalar){
	TCC0.CTRLA = prescalar;
}

int pwm_begin(ATK_PWM_CHANNEL_sel channel, TC_CLKSEL_t prescalar, int desiredFrequency){

	PORTC_DIRSET |= channel>>4;

	TCC0.CTRLA = prescalar; //prescalar 1
	//TCC0.CTRLA = TC_CLKSEL_DIV1_gc;
	TCC0.CTRLB = TC_WGMODE_SINGLESLOPE_gc | channel;//wave generation mode single slope, enabling CCA open port OC
	//TCC0.CTRLB = TC_WGMODE_SINGLESLOPE_gc | 1<<4;//wave generation mode single slope, enabling CCA open port OC
	
	int prescalardiv= pow(2,prescalar-1);//gets division from prescalar

	per = (ATK_BASE_FREQUENCY/prescalardiv)/(desiredFrequency); //  x = (BF/p)/DF Base Frequency, prescalar, desired frequency
	per = 1920;
	if(per >65535 || per<0){
		return -1;
	}
	uint8_t perl = (uint8_t) per;
	uint8_t perh = (uint8_t) (per >> 8);
	
	TCC0.PERBUFL = perl;//for the waveform period - controls TOP value
	TCC0.PERBUFH = perh;

	set_pwm(channel,0);
	return 0;
}