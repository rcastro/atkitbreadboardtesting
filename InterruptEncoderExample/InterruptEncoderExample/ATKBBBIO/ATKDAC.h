/*
 * ATKDAC.h
 *
 * Created: 10/7/2018 2:06:40 PM
 *  Author: Ruben Castro
 */ 


#ifndef ATKDAC_H_
#define ATKDAC_H_


//for ATKBBB - 0 is PB2, and 1 is PB3
typedef enum ATK_DAC_CHANNEL_enum{
	ATK_DAC_CHANNEL_0 =0x01<<0,
	ATK_DAC_CHANNEL_1 =0x01<<1,
	ATK_DAC_DUAL_CHANNEL =0x02<<0,
} ATK_DAC_CHANNEL_sel;


void dac_begin( ATK_DAC_CHANNEL_sel channel);
void set_dac( ATK_DAC_CHANNEL_sel channel, uint16_t val); //set 12 bit dac value [0,4096)


#endif /* ATKDAC_H_ */