/*
 * l_filter.h
 *
 * Created: 10/11/2018 1:46:46 PM
 *  Author: Ruben Castro
 */ 


#ifndef L_FILTER_H_
#define L_FILTER_H_

#include <avr/io.h>

typedef struct{
	uint8_t K;
	int32_t filter_reg;
}l_filter_t;

void l_filter_init(l_filter_t *filter, uint8_t K);
uint16_t l_filter(l_filter_t *filter, uint16_t input);


#endif  // L_FILTER_H_