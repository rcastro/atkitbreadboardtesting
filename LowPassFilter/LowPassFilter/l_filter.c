/*
 * l_filter	.c
 *
 * Created: 10/11/2018 1:47:09 PM
 *  Author: Ruben Castro
 */ 

 #include "l_filter.h"
 #include <avr/io.h>

 uint16_t l_filter(l_filter_t *filter, uint16_t input){
	filter->filter_reg = (filter->filter_reg) - ((filter->filter_reg) >> filter->K) + input>>filter->K; // y(n) = (1-2^-k)(y(n-1)) + (2^-k)(x(n))
	return filter->filter_reg;	
 };

 
 void l_filter_init(l_filter_t *filter, uint8_t K){
	filter->filter_reg=0;
	filter->K = K;
 }


