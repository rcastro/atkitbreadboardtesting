

/*
 * DACExample for Atxmega256a3u
 *
 * Created: 10/2/2018 6:08:30 PM
 * Author :  Ruben Castro
 */ 


#include <avr/io.h>

const char SineTable64[] = {
	127, 139, 152, 164, 176, 187, 198, 208, 217, 225, 233, 239, 244, 249, 252, 253,
	254, 253, 252, 249, 244, 239, 233, 225, 217, 208, 198, 187, 176, 164, 152, 139,
	127, 115, 102, 90, 78, 67, 56, 46, 37, 29, 21, 15, 10, 5, 2, 1,
	0, 1, 2, 5, 10, 15, 21, 29, 37, 46, 56, 67, 78, 90, 102, 115
};

void set_dac(uint16_t val){ //set 12 bit dac value
	
//	uint8_t lo = val;
	//uint8_t hi = val<<8;
	
	//DACB.CH0DATAL = lo;
	//DACB.CH0DATAH = hi;
	while(!(DACB.STATUS & DAC_CH0DRE_bm));
	DACB.CH0DATA = val;



}

void dac_begin(){
	PORTB.DIRSET = PIN2_bm;
	
	DACB.CTRLA = DAC_CH0EN_bm | DAC_ENABLE_bm; //enable output pin 1 - PB0
	DACB.CTRLB = DAC_CHSEL_SINGLE_gc;
	DACB.CTRLC = DAC_REFSEL_AVCC_gc;

	set_dac(1024);

}


int main(void)
{
	dac_begin();

    	uint16_t tck = 1;
    	uint16_t counter = 1;
    	while (1)
    	{
	    	tck++;
	    	if(!(tck%30)){
		    	counter++;
		    	if(counter>63){
			    	counter=0;
		    	}
		    	set_dac(SineTable64[counter]*10);
	    	}
	    	
	    	

    	}
}
