/*
 * PWMControl 
 * main.c
 *
 * Created: 10/2/2018 12:12:14 PM
 * Author : Jake Read, Ruben Castro
 */ 


#include <avr/io.h>

void clock_init(void){
	OSC.XOSCCTRL = OSC_XOSCSEL_XTAL_256CLK_gc | OSC_FRQRANGE_12TO16_gc; // select external source
	OSC.CTRL = OSC_XOSCEN_bm; // enable external source
	while(!(OSC.STATUS & OSC_XOSCRDY_bm)); // wait for external
	OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC0_bm | OSC_PLLFAC1_bm; // select external osc for pll, do pll = source * 3
	//OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC1_bm; // pll = source * 2 for 32MHz std clock
	OSC.CTRL |= OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL for main clock
}


void set_pwm(uint16_t val){ //setPWM to value between 0 and 40960
	uint8_t vall = (uint8_t) val;
	uint8_t valh = (uint8_t) (val >> 8);
	
	TCC0.CCABUFL = vall; //hi and lo compare capture channels
	TCC0.CCABUFH = valh;
}

void pwm_begin(){
	PORTC.DIRSET = PIN0_bm;
	
	TCC0.CTRLA = TC_CLKSEL_DIV1_gc; //prescalar 1
	TCC0.CTRLB = TC_WGMODE_SINGLESLOPE_gc | (1 << 4);//wave generation mode single slope, enabling CCA open port OC
	uint16_t base = 48000;
	uint16_t desired = 20;
	uint16_t prescalardiv =1;

	uint16_t per = (base/prescalardiv)/desired; //  x = (BF/p)/DF Base Frequency, prescalar, desired frequency 
	//uint16_t per = 2000;
	uint8_t perl = (uint8_t) per;
	uint8_t perh = (uint8_t) (per >> 8);
	
	TCC0.PERBUFL = perl;//for the waveform period - controls TOP value
	TCC0.PERBUFH = perh;

	set_pwm(2048);
}


int main(void)
{
	clock_init();

	pwm_begin();
	
	// 'pin' 40 / port e - pin 3 
    PORTE.DIRSET = PIN3_bm;
	PORTE.DIRSET = PIN2_bm;
	uint16_t tck = 1;
	uint16_t counter = 1;
    while (1) 
    {
		tck++;
		if(!(tck%1000)){
			counter++;
			if(counter>2398){
				counter=0;
			}	
			set_pwm(50);
		}
		
		

    }
}
