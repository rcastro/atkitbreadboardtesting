/*
* RPID.c
*
* Created: 10/7/2018 1:22:31 PM
*  Author: Ruben Castro
*/

#include "avr/io.h"
#include "RPID.h"
#include "l_filter.h"
#include "ATKBBBIO/RBasic.h"
#include "ATKBBBIO/ATKDAC.h"
#include <stdlib.h>


double RPID_crunch(RPID_t *pid, double input){
	double pValue, iValue, dValue;
	pid->currentError = pid->setpoint - input;
	if(pid->CONT){
		if(abs(pid->currentError) > (pid->MAX_IN - pid->MIN_IN)/2){
			pid->currentError  = pid->currentError>0? pid->currentError- pid->MAX_IN + pid->MIN_IN : pid->currentError+ pid->MAX_IN- pid->MIN_IN;
		}
	}

	if(pid->currentError * pid->P < pid->MAX_OUT && pid->currentError * pid->P > pid->MIN_OUT){
		
		pid->totalError+=pid->currentError;
		}else{
		pid->totalError=0;
	}

	//pValue = Math.abs(currentError) < acceptableRange ? 0: p * currentError;
	pValue = pid->P * pid->currentError;
	iValue = pid->I * pid->totalError * pid->dt;
	dValue = l_filter(&pid->filter,  (pid->currentError - pid->prevError) / pid->dt) ; // filter d, since it would look like a step function at a really fast frequency
	//dValue /=10;
	int dSub =  dValue;
	dSub = dSub *  pid->D;
	dValue = dSub;
	//set_dac(ATK_DAC_CHANNEL_0, dValue +2048);
	//dValue = 0;

	pid->prevError = pid->currentError;
	//return pValue + iValue + dValue;
	
	//check if on target
	if(abs(pid->currentError) < pid->acceptableRange){
		pid->onTargetTicks++;
		}else{
		pid->onTargetTicks=0;
	}


	return CONSTRAIN(pValue + iValue + dValue,pid->MIN_OUT, pid->MAX_OUT);
}


void RPID_setSetpoint(RPID_t *pid, double val){
	pid->setpoint=val;
}

void RPID_setAcceptableRange(RPID_t *pid, double range){
	pid->acceptableRange=range;
}

uint8_t RPID_onTarget(RPID_t *pid){ // if on target for more than 50 millis

	return pid->onTargetTicks * pid->dt >0.15;
}


void RPID_init(RPID_t *pid, double P, double I, double D, double dt,
uint8_t CONT, int MIN_IN, int MAX_IN, int MIN_OUT, int MAX_OUT, uint8_t K){
	pid->P = P;
	pid->I=I;
	pid->D=D;
	pid->dt=dt;
	pid->CONT=CONT;
	pid->MIN_IN=MIN_IN;
	pid->MAX_IN=MAX_IN;
	pid->MIN_OUT=MIN_OUT;
	pid->MAX_OUT=MAX_OUT;
	
	pid->prevError=0;
	pid->currentError=0;
	pid->totalError=0;
	pid->setpoint=0;
	pid->acceptableRange=50;

	pid->onTargetTicks=0;

	l_filter_init(&pid->filter, K);
}