/*
 * ATKEncoder.c
 *
 * Created: 10/8/2018 1:45:19 PM
 *  Author: Ruben Castro
 */ 
 #include <avr/interrupt.h>
 #include <avr/io.h>


int counter;
double tPV;

void ATKEncoder_begin(double tickPerV){

	PORTC_DIRSET &= ~(PIN2_bm | PIN1_bm);

	PORTC.INTCTRL = PMIC_HILVLEN_bm;

	//interrupt 0 PC1 falling edge - doesnt work?
	PORTC.INT0MASK = PIN1_bm;
	PORTC.PIN1CTRL = PORT_ISC_FALLING_gc;
	
	//interrupt 1 PC2 rising edge
	PORTC.INT1MASK = PIN2_bm;
	PORTC.PIN2CTRL = PORT_ISC_RISING_gc;


	counter =0;
	tPV = tickPerV;
}

int ATK_EN_getCounter(){
	return counter;
}

double ATK_EN_getValue(){
	return (double)counter/tPV;
}
 

//PC1 is Rising -1
ISR(PORTC_INT0_vect)
{
	if(!(PORTC_IN & PIN2_bm)){
		counter+=10;
		}else{
		counter-=10;
	}
}

 //PC2 is falling -0
 ISR(PORTC_INT1_vect)
 {
	 if((PORTC_IN & PIN1_bm)){
		 counter+=10;
		 }else{
		 counter-=10;
	 }
 }
