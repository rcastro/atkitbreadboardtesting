/*
* PIDMotorController.c
*
* Created: 10/7/2018 12:25:29 PM
* Author : Ruben Castro
*/

#define OTHERSETPOINT 500

#include <avr/io.h>
#include <avr/interrupt.h>
#include "RPID.h"
#include "pin.h"
#include "ATKBBBIO/ATKDAC.h"
#include "ATKBBBIO/ATKCLK.h"
#include "r_adc.h"
#include "motor.h"
#include <stdlib.h>
#include "pwm.h"
#include "RBasic.h"
#include "current_controller.h"
#include "ATKBBBIO/ATKEncoder.h"

pin_t M_INA;//H bridge A
pin_t M_INB;//H bridge B
pwm_t M_PWM;//H bridge pwm val

motor_t M_1;

r_adc_t ADC_CS;//current sensing ADC
l_filter_t CS_filter;

RPID_t M_1_pid;
currentc_t CC;


int main(void)
{
	//initialize Clock to 48 Mhz
	clock_init();

	//initialize interrupts
	sei();
	PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

	PORTE.DIRSET |= PIN3_bm | PIN2_bm;


	
	//start DAC, PWM, and Encoder
	dac_begin(ATK_DAC_DUAL_CHANNEL);
	ATKEncoder_begin(1);//1 tick/unit

	
	//initialize motor H bridge pins
	PORTC.DIRSET |= PIN0_bm || PIN4_bm || PIN3_bm;
	PORTC_DIRSET |= PIN5_bm;
	PORTC_OUTSET |= PIN5_bm;
	pin_init(&M_INA, &PORTC, PIN4_bm,1,1);
	pin_init(&M_INB, &PORTC, PIN3_bm,0,1);
	pwm_init(&M_PWM, &TCC0,TC_CLKSEL_DIV1_gc,TC0_CCAEN_bm,48000,10);

	motor_init(&M_1,&M_INA,&M_INB,&M_PWM);
	
	//initialize current controller
	PORTB_DIRCLR |= PIN0_bm; 
	r_adc_init(&ADC_CS, &ADCB, &ADCB.CH0, ADC_CH_MUXPOS_PIN0_gc);
	currentc_init(&CC, 2.0,8.0,0.0005,0,5000,0,100);
	currentc_setSetpoint(&CC, OTHERSETPOINT);
	currentc_setAcceptableRange(&CC, 50);
	l_filter_init(&CS_filter, 6);
	



	//initialize PID
	RPID_init(&M_1_pid,5.0,0.0,  0.0   ,0.00016,0, 0,0,-300,300, 6);

	//initialize debugging DACs
	PORTB.DIRSET |= PIN2_bm | PIN3_bm;
	//set_dac(ATK_DAC_CHANNEL_0, 1000);
	//set_dac(ATK_DAC_CHANNEL_1, 1000);


	pwm_set(&M_PWM,1000);
	
	PORTE_DIRSET = PIN2_bm | PIN3_bm;
	RPID_setSetpoint(&M_1_pid, 46);

	//initialize tickets
	timing_interrupts_init();


	while(1){
		
	}

}

void timing_interrupts_init(){
	TCC1.CTRLA = TC_CLKSEL_DIV1_gc;

	uint16_t pera = 24000; //  2khz (48MHZ/24000)
	uint8_t peral = (uint8_t) pera;
	uint8_t perah = (uint8_t) (pera >> 8);

	TCC1.PERBUFL = peral;
	TCC1.PERBUFH = perah;

	TCC1.INTCTRLA = TC_OVFINTLVL_MED_gc;
}


	//used to update current controller

	


int i=0;

ISR(TCC1_OVF_vect){
	i++;
	if(!(i%1000)){ //blink per second
		PORTE_DIRTGL = PIN2_bm;
	}

	//pwm_set_dutycycle(&M_PWM,25);
	
	//set_dac(ATK_DAC_CHANNEL_0,  +256);
	//set_dac(ATK_DAC_CHANNEL_0, -M_1_pid.filter.filter_reg +2048);
	//set_dac(ATK_DAC_CHANNEL_0, 2048);
	
	//set_dac(ATK_DAC_CHANNEL_0, M_1_pid.setpoint);

	//set_dac(ATK_DAC_CHANNEL_0, ATK_EN_getValue() +256);
	//set_dac(ATK_DAC_CHANNEL_1, M_1_pid.setpoint + 256);
	//set_dac(ATK_DAC_CHANNEL_1, CC.setpoint);


	//16.8763 degrees to side  977 ticks per rev 2.7ticks per degree 36 is 0 degree 290 is 90deg
	if(RPID_onTarget(&M_1_pid)){
		//PORTE_DIRTGL = PIN3_bm;
		if(M_1_pid.setpoint == OTHERSETPOINT){
			RPID_setSetpoint(&M_1_pid, 30);
			}else{
			RPID_setSetpoint(&M_1_pid, OTHERSETPOINT);
		}
	}
	double V_val = RPID_crunch(&M_1_pid, ATK_EN_getValue());
	V_val = 100;
	currentc_setSetpoint(&CC, abs(V_val));

	//if(currentc_onTarget(&CC) ){
		//PORTE_DIRTGL = PIN3_bm;
		//if(CC.setpoint==OTHERSETPOINT){
			//currentc_setSetpoint(&CC, 50);
			//}else {
			//currentc_setSetpoint(&CC, OTHERSETPOINT);
		//}
	//}

	if(currentc_onTarget(&CC)){
		PORTE_OUTCLR = PIN3_bm;
	}else{
		PORTE_OUTSET = PIN3_bm;
	}

	//value of 4096 is 3.3v
	//uint16_t input = l_filter(&CS_filter, r_adc_scan(&ADC_CS));
	int unfiltered = r_adc_scan(&ADC_CS)*5.75;
	uint16_t input = l_filter(&CS_filter, unfiltered);//3300mV/ 4096units * unitsmeasured / 0.140mV/mA == MilliAMPS being pulled
	//int input =100;
	//set_dac(ATK_DAC_CHANNEL_0,  input);
	double C_val = currentc_crunch(&CC, input);//value of 4096 is 3.3v
	//set_dac(ATK_DAC_CHANNEL_1,  C_val*31 + 620);//1% = 25mV
	set_dac(ATK_DAC_CHANNEL_1,  unfiltered*6 + 620);//100mA = 250mV
	set_dac(ATK_DAC_CHANNEL_0,  input*6 + 620);//1mA = 5mV
	//set_dac(ATK_DAC_CHANNEL_0, CONSTRAIN(-20, 0,4096)*20 +620);
	//set_dac(ATK_DAC_CHANNEL_1,  620);//100mA = 250mV

	//motor_set(&M_1, C_val); //use for current control only mode
	//motor_set(&M_1, V_val >0? C_val:-C_val);//use for position mode
	motor_set(&M_1, 40);

	//set_dac(ATK_DAC_CHANNEL_1, dac2);
	//set_dac(ATK_DAC_CHANNEL_1, input/5.75 + 124);

//
	//set_dac(ATK_DAC_CHANNEL_0, 3000); //0 is blue 1 is red
	//set_dac(ATK_DAC_CHANNEL_1, 500);


}

