/*
 * motor.h
 *
 * Created: 10/8/2018 2:33:15 PM
 *  Author: raque
 */ 


#ifndef MOTOR_H_
#define MOTOR_H_

#include "pin.h"
#include "pwm.h"

typedef struct{
	pin_t *INA_pin;
	pin_t *INB_pin;
	pwm_t *PWM;
}motor_t;

void motor_init(motor_t *motor, pin_t *INA_pin, pin_t *INB_pin, pwm_t *PWM);

void motor_set(motor_t *motor, double val);
void motor_stop(motor_t *motor);




#endif /* MOTOR_H_ */